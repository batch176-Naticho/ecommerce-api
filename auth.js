const jwt = require('jsonwebtoken');
const secret = 'Ecommerce'
module.exports.createAccessToken = (userData) => {
	const data = {
		id: userData._id,
		email: userData.email,
		isAdmin: userData.isAdmin
	}
	return jwt.sign(data,secret,{})
}

//Token Verification
module.exports.verify = (req,res,next) => {
	let token = req.headers.authorization;
	if(typeof token === "undefined"){
		return res.send({message: "failed. No token"})
	}else{
		token = token.slice(7, token.length)

		jwt.verify(token, secret, function (error, decodedToken) {
			if (error) {
				res.send({
					auth:"Failed",
					message: error.message
				})
			} else {
				req.user = decodedToken
				next();
			}
		})
	}
};

module.exports.verifyAdmin = (req,res,next) => {
	if (req.user.isAdmin) {
		next();
	} else {
		return res.send({
			auth:"Failed",
			message: "You are not an Admin"
		})
	}
};
