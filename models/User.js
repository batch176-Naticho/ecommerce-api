//[SECTION] Modules and Depencies
	const mongoose = require('mongoose');
	
//[SECTION] Schema/Blueprint
	const userSchema = new mongoose.Schema({
		email:{
			type:String,
			required:true
		},
		password:{
			type:String,
			required:true
		},
		isAdmin:{
			type:Boolean,
			default:false
		}
	})
	
//[SECTION] Model
	module.exports = mongoose.model('User',userSchema)