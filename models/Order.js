//[SECTION] Modules and Depencies
	const mongoose = require('mongoose');
	
//[SECTION] Schema/Blueprint
	const orderSchema = new mongoose.Schema({
		userId:{
			type:String,
			required: [true, 'User Id is Required']
		},
		products:[
			{
				productId: {
					type:String,
					required: [true, 'Product Id is Required']
				},
				quantity:{
					type:Number,
					required:[true, 'Quantity is Required']
				}
			}
		],
		totalAmount:{
			type:Number,
			default: 0
		},
		purchasedOn:{
			type:Date,
			default:new Date()
		}
	});
	
//[SECTION] Model
	module.exports = mongoose.model('Order',orderSchema)