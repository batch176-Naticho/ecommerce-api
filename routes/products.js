const express = require('express');
const route = express.Router();
const ProductController = require('../controller/products');
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

//[POST] Route for Creating a Product
route.post('/product', verify,verifyAdmin,(req,res) => {
	ProductController.addProduct(req.body).then(result => res.send(result))
});

//[GET] Retreive All Products
route.get('/allProducts', (req,res) => {
	ProductController.getAllProducts().then(result => res.send(result))
})

//[GET] Route for Retreiving All Active Products
route.get('/allActive', (req,res) => {
	ProductController.getAllActive().then(result => res.send(result))
}); 

//[GET] Route for Retreiving a Single Product
route.get('/:productId', (req,res) => {
	ProductController.getSingleProduct(req.params.productId).then(result => res.send(result))
});

//[PUT] Route to Update Product Information
route.put('/:productId', verify,verifyAdmin, (req,res) => {	
	ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
});

//[PUT] Route for Archiving a Product
route.put('/:productId/archive',verify,verifyAdmin, (req,res) => {
	ProductController.archiveProduct(req.params.productId).then(result => res.send(result))
});

//[PUT] Activate a Product
route.put('/:productId/activate',verify,verifyAdmin, (req,res) => {
	ProductController.activateProduct(req.params.productId).then(result => res.send(result));
});

//[DEL] Route for deleting a product
route.delete('/:productId/delete', verify,verifyAdmin, (req,res) => {
	ProductController.deleteProduct(req.params.productId).then(result => res.send(result))
});





//[SECTION] Expose Routing System
module.exports = route