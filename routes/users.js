//[SECTION] Depencies and Modules
	const express = require('express')
	const controller = require('../controller/users')	
	const auth = require('../auth')
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
	const route = express.Router();
		
//[POST] Routes to create user 
	route.post('/register', (req,res) => {
		let userInfo = req.body;
		controller.register(userInfo).then(result=>{
			res.send(result)
		})
	});
//[POST] Route for User Authentication (Login)
	route.post('/login', (req,res) => {
		controller.loginUser(req.body).then(result => res.send(result))
	})

//[GET] Route for Retreiving User's Details
route.get('/details',auth.verify,(req,res) => {
	controller.getUsersDetail(req.user.id).then(result => res.send(result))
})

//[GET] Route for Retreiving All Users
route.get('/allUsers',verify, verifyAdmin, (req,res) => {
	controller.getAllUsers().then(result => res.send(result))
})

//[PUT] Route for Setting User as Admin
route.put('/:toAdmin',verify,verifyAdmin, (req,res) => {
	let userId = req.params.toAdmin;
	controller.userToAdmin(userId).then(result => {
		res.send(result)
	});
})
	
//[SECTION] Expose Routing System
	module.exports = route;
	