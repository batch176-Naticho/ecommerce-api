//[SECTION] Depencies and Modules
const express = require('express');
const OrderController = require('../controller/orders')
const route = express.Router();
const auth = require('../auth');
const {verify, verifyAdmin} = auth;


//[POST] Route for Creating Order
route.post('/createOrder',verify, (req,res) => {
	OrderController.addOrder(req.body).then(result => res.send(result))
});

//[GET] Route for Retreiving All Orders
route.get('/allOrders',verify,verifyAdmin,(req,res) => {
	OrderController.getAllOrders().then(result => res.send(result))
});

//[GET] Retreive Authenticated User's Order
route.get('/:authUserOrder',verify, (req,res) => {
	OrderController.getAuthOrder(req.params.userId).then(result => res.send(result))	
});

//[SECTION] Expose Routing System
module.exports = route