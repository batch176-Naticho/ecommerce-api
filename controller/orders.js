//[SECTION] Depencies and Modules
const Product = require('../models/Product');
const User = require('../models/User');
const Order = require('../models/Order');
const product = require('./products')


//[POST] Create Order
module.exports.addOrder = (reqBody) => {
	let totalCost = reqBody.quantity*reqBody.price
	let newOrder = new Order ({
		userId: reqBody.userId,
		products: [
			{
				productId: reqBody.productId,
				quantity: reqBody.quantity
			}
		],
		totalAmount: totalCost
	});
	return newOrder.save().then((order,error) => {
		if (order) {
		return true
		} else {
	 		return false
	 	}
	 })
};

//[GET] Retreive Authenticated User's Order
	module.exports.getAuthOrder = (reqParams) => {
		return Order.findById(reqParams).then(result => {
			return result
		})
	};

//[GET] Retreive All Orders
module.exports.getAllOrders = () => {
	return Order.find({}).then(result => {
		return result
	})
};



