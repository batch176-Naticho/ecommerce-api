const Product = require('../models/Product');


//[POST] Create a Poduct
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
//Save
	return newProduct.save().then((product,error) => {
		if (product) {
			return true
		} else {
			return false
		}
	}).catch(error => error.message)

};

//[GET] Retreive All Products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
}


//[GET] Retreive All Active Products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result
	})
};

//[GET] Route to Retreive a Single Product
module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	})
};

//[PUT] Update Product Information
module.exports.updateProduct = (productId, data) => {
	let updatedProduct = {
	  name: data.name,
      description: data.description,
      price: data.price
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		if (product) {
			return true
		} else {
			return false
		}
	})
};

//[PUT] Archiving a Product
module.exports.archiveProduct = (productId) => {
	let updatedField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(productId, updatedField).then((product,error) => {
		if (product) {
			return true
		} else {
			return false
		}
	})
};


//[PUT] Activating a Product
module.exports.activateProduct = (productId) => {
	let updateActiveField = {
		isActive: true
	};

	return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
};

//[DELETE] Delete a Product
module.exports.deleteProduct = (productId) => {
	return Product.findById(productId).then((product, error) => {
		if (product) {
			return true
		} else {
			return false
		}
	})
};

