//[SECTION] Depencies and Modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require ('../auth.js');
	
//[SECTION] Environment Variables Setup
	dotenv.config();
	const salt = Number(process.env.SALT);
	
//[GET] Create User 
	module.exports.register = (userInfo) => {
		let email = userInfo.email;
		let pWord = userInfo.password;

		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(pWord, salt)
		});
		return newUser.save().then((user, error) => {
			if (user) {
				return true
			} else {
				return false
			}
		})
	};
	
//[SECTION] User Authentication	(Login)
module.exports.loginUser = (data) => {
	return User.findOne({email: data.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const PasswordCorrect = bcrypt.compareSync(data.password, result.password)
			if (PasswordCorrect) {
				return {accessToken: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}	
		}
	})
};

//[GET] Retreive All Users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result
	})
};

//[GET] Retreive User's Details
module.exports.getUsersDetail = (data) => {
	return User.findById(data).then(result => {
		result.password = '';
		return result;
	})
}

//[PUT] Setting User as Admin
module.exports.userToAdmin = (userId) => {
	return User.findById(userId).then((result,error) => {
		if (result) {
			result.isAdmin = true
			return result.save().then((updated,error) => {
				if (updated) {
					return true
				} else {
					return false
				}
			});
		} else {
			return false

		}
	})
};
